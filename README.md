Recommendations for infrastructure & operations
-----------------------------------------------

### Tech stack

Unity3D, ARKit, iOS 11 or above.

### ARKit or ARCore?

Our early tests with ARCore (on the Pixel 2) showed poor performance when compared with ARKit (on an iPhone X). Apple is often at an advantage due to the fact that they can build their software to the exact specifications of the target devices. ARkit seems to be no exception to the rule. For that reason, at the time of this writing (July 2018), we would recommend iOS over Android for the best performance with mobile AR. That being said, AR is progressing fast, and ARKit's advantage will likely vanish as machines become more powerful.

### Which device?

This is a list of devices that support ARKit (more recent models of iPhone/iPad should also be compatible).

![](img/arkitCompatibleDevices.jpg)

We ran some informal tests for ARKit's tracking performance (comparing an iPhone X and a 6th generation iPad with a custom build Unity app). More rigorous tests would be needed to conclude with certainty, but our preliminary result seem to show that the tracking performance is inconsistent across devices. This is something to consider when picking a device for your installation. More processing power is better.

![The two devices were moved along the same path, but the line on the iPad looks broken in many points, where the tracking failed. The line on the iPhone is looking smoother.](img/trackingTest.png "ARKit tracking test: iPad 6th gen VS iPhone X")

### On battery life

Augmented Reality is a real battery drain. It's not uncommon for a fully charged iPhone to run out of battery after 45 minutes of uninterrupted AR use. If your installation is supposed to run continuously for a long time, consider having more devices than you need so they can be charged in turns.

### Putting the device in kiosk-mode

In most cases, you will want to prevent users from leaving the app. Here's [a useful guide](https://www.webascender.com/blog/setup-kiosk-mode-lock-ipad-just-one-app/) on how to achieve that. In case you need a more granular solution, [restrictions](https://support.apple.com/en-us/HT201304#restrictions-on-off) are where you want to look. Restrictions let you define one by one which apps, features, and services are accessible on the device.

### Configuring devices

Configuring a large number of devices manually is a pain. You need to create iCloud accounts for each and keep track of passwords. Alternatively, you can use [Apple Configurator](https://support.apple.com/apple-configurator) to quickly configure large numbers of devices with the settings, apps, and data you specify.

### Getting the devices back

A top of the line iPad or iPhone is an expensive thing to be freely handing out to people in a crowded space. It is not legal in Europe to ask that people leave their ID. As an alternative, you can ask them to hand out their own phone as a deposit.

### What if a device get stolen anyway?

You can install [Find My iPhone](https://support.apple.com/en-us/HT201472#FindMy) to track the device if it gets stolen. It may not help recover it, but the app lets you lock the device remotely.

### Making the app available for download (mind the review time!)

Publishing your app on the Apple App Store is not exactly as straightforward as clicking a button. Before others can download your app, you need to go through Apple's infamous review process. Fortunately, a lot of it seems to have been automated these days and it only takes between 1 and 6 days for most apps. It is a good idea to read the [official guidelines](https://developer.apple.com/support/app-review/) and check out [this handy website](http://appreviewtimes.com/) for info on average review times (pro tip: review times peak during the holidays). Schedule for your app to be done a couple of weeks before (in case it doesn't pass the first review) if you plan to make your app available to download.

Design choices
--------------

Arm strain: avoid forcing the user to hold the device up (long animations or floating text).

Eye/hand conflict: aiming with one hand while tapping with the other is a challenge, especially with larger and heavier devices.

Markers: The maximum amount of markers is 30. Should the need arise, we could dynamically load sets -> e.g. pick key paintings that will trigger the loading of a set of markers for nearby paintings.

Tablet VS phone: though phones are easier to handle, we went for tablets on the assumption that they would be better for groups.

Headphones: wearing headphones tend to isolate people from each other, while text and images distract them from looking at the art.

iPad Setup instructions
-----------------------

### Configure the first iPad manually

1.  Settings > General > Language & Region : set to English

2.  Settings > General > About > Name : Rename into "iPad XX" where XX stands for the unique ID of the iPad

3.  Settings > Location : Turn "Location Services" ON

4.  Settings > General > Airdrop : Set to "Everyone"

5.  Airdrop wallpapers to the iPad and set Lock and Home screen

6.  Compile the app to the device

7.  Settings > General > Multitasking & Dock : Turn "Show Suggested and Recent Apps" OFF

8.  Hide icons to second screen and leave only our App on the first screen

9.  Lock rotation

10. [Optional] Set up "Find my iPhone"

11. [Optional] General > Restrictions : Enable Restrictions (set ones that apply)

12. General > Accessibility > Guided access : ON and Passcode Settings "Set Guided Access Passcode" (Keep the code in a safe place)

### Create and archive of the configured iPad using Apple Configurator 2

1.  Plug the first iPad in the Computer

2.  On the iPad, "Trust this

3.  Launch Apple configurator 2

4.  Select the iPad and click "Back Up"

### Configure another iPad using Apple Configurator 2

1.  Go to Actions > Restore from backup

2.  Select the backup and click on "Restore"

### Finish Setting up

1.  Connect to a WiFi

2.  Don't set a passcode: tap on "Passcode Options" and select "Don't use passcode"

3.  Log in with an Apple ID (or tap on "Forgot password or don't have an Apple ID?" then "Set up Later in Settings")

4.  Enable Location Services

5.  Settings > General > About > Name : Rename into "iPad XX" where XX stands for the unique ID of the iPad

6.  Set the wallpapers for the lock screen and the home screen

7.  Compile the app to the iPad

### On the day of the event

1.  Remove the case and store it away

2.  Add the M4.0 sticker to the back of the actual iPad
