﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class PlaceOnLastDetected : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AnchorAdded;
	}
	
	void AnchorAdded(ARImageAnchor arImageAnchor){
		Vector3 position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
		transform.position = position;
	}

	void OnDestroy(){
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AnchorAdded;
	}
}
