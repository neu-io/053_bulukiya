﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartManager : MonoBehaviour {

    public GameObject _mainCanvas;
    public GameObject[] _switchers;

        void Start()
        {
            foreach (GameObject s in _switchers)
            {
                s.SetActive(false); // hide all switchers on Start()
            }

            _mainCanvas.SetActive(true);// show main canvas on start
        }
      
}
