﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : MonoBehaviour {
	static List<PointOfInterest> poiList = new List<PointOfInterest>();

	public List<GameObject> _objects;

	public static void Reset(){
		foreach(var poi in poiList){
			poi.ResetPoi();
		}
	}
	public void Activate(){
		foreach(var poi in poiList){
			if(poi == this){
				continue;
			}
			poi.Deactivate();
		}
		foreach(var obj in _objects){
			obj.SetActive(true);
		}
		gameObject.SetActive(false);
	}
	public void ResetPoi(){
			
		foreach(var obj in _objects){
			obj.SetActive(false);
		}
		gameObject.SetActive(true);
	}

	public void Deactivate(){
		foreach(var obj in _objects){
			obj.SetActive(false);
		}
		gameObject.SetActive(false);
	}
	public void DeactivateAll(){
		foreach(var poi in poiList){
			if(poi == this){
				continue;
			}
			poi.Deactivate();
		}
	}
	

	void Start(){
		poiList.Add(this);
		ResetPoi();
	}
	void OnDestroy(){
		poiList.Remove(this);
	}
}
