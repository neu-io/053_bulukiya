﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {
	public GameObject _paintintro;

	public Text _title;
	public Text _subtitle;
	public Text _body;
	public Image _image;
	public GameObject _audio;

	public void SetCard(CardSettings card){
		_title.text = card.title;
		_subtitle.text = card.subtitle;
		_body.text = card.body;
		_image.sprite = card.image;

		var audioSource = _audio.GetComponent<AudioSource>();
		audioSource.clip = card.audio;
		if(LanguageSelector.language == LanguageSelector.Lang.De){
			_title.text = card.title_de;
			_subtitle.text = card.subtitle_de;
			_body.text = card.body_de;
			audioSource.clip = card.audio_de;
		}
		audioSource.Play();

		_paintintro.SetActive(false);
	}
}
