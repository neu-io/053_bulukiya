﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switcher : MonoBehaviour {
	static List<Switcher> switcherList = new List<Switcher>();

	public static void Reset(){
		foreach(var switcher in switcherList){
			switcher.ResetSwitcher();
		}
	}

	public void Activate(){
		foreach(var switcher in switcherList){
			if(switcher == this){
				continue;
			}
			switcher.Deactivate();
		}
		gameObject.SetActive(true);
	}

	public void ResetSwitcher(){
		gameObject.SetActive(true);
	}

	public void Deactivate(){
		gameObject.SetActive(false);
	}
	public void DeactivateAll(){
		foreach(var switcher in switcherList){
            if(switcher == this){
				continue;
			}
            switcher.Deactivate();
		}
	}
	

	void Start(){
		switcherList.Add(this);
		ResetSwitcher();
	}
	void OnDestroy(){
		switcherList.Remove(this);
	}
}
