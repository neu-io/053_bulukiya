using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SetCard))]
public class SetCardEditor : Editor
{
 public override void OnInspectorGUI()
 {
  DrawDefaultInspector();

  SetCard intro = (SetCard)target;
  if (GUILayout.Button("Show"))
  {
   intro.Set();
  }
 }
}