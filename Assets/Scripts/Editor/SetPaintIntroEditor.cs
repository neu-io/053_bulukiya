using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SetPaintIntro))]
public class SetPaintIntroEditor : Editor
{
 public override void OnInspectorGUI()
 {
  DrawDefaultInspector();

  SetPaintIntro intro = (SetPaintIntro)target;
  if (GUILayout.Button("Show"))
  {
   intro.Set();
  }
 }
}