﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintIntro : MonoBehaviour {
	public GameObject _tutorial;
	public GameObject _tutorial_de;

	public Text _title;
	public Text _subtitle;
	public Text _body;
	public Image _image;



	public void SetPaintIntro(PaintIntroSettings PaintIntro){
		_title.text = PaintIntro.title;
		_subtitle.text = PaintIntro.subtitle;
		_body.text = PaintIntro.body;
		_image.sprite = PaintIntro.image;
		if(LanguageSelector.language == LanguageSelector.Lang.De){
			_title.text = PaintIntro.title_de;
			_subtitle.text = PaintIntro.subtitle_de;
			_body.text = PaintIntro.body_de;
		}

		_tutorial.SetActive(false);
		_tutorial_de.SetActive(false);
	}
	public void ResetPOIS(){
		PointOfInterest.Reset();
	}
}
