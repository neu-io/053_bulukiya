﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioProgress : MonoBehaviour {
	public AudioSource _audio;

	RectTransform trans;

	void Start(){
		trans = GetComponent<RectTransform>();
	}

	void Update(){
		if(_audio.clip == null){
			return;
		}

		var t = _audio.time;

		var dur = _audio.clip.length;

		trans.anchorMax = new Vector2(t/dur,trans.anchorMax.y);


	}
}
