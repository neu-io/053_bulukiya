﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCard : MonoBehaviour{
	public Card _targetCard;
	public CardSettings _card;

	public void Set(){
		_targetCard.gameObject.SetActive(true);
		_targetCard.SetCard(_card);
	}
	public void Disable(){
		_targetCard.gameObject.SetActive(false);
		_targetCard.SetCard(_card);
	}
}
