﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelector : MonoBehaviour {
	public enum Lang{
		En,
		De
	}
	public static Lang language;
	public void SetEnglish(){
		language = Lang.En;
	}
	public void SetGerman(){
		language = Lang.De;
	}
}
