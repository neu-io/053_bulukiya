using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "CardSettings" , menuName = "NEEEU/Card", order = 1)]
public class CardSettings : ScriptableObject {
	public string title;
    public string title_de;


    [Multiline]
    public string subtitle;
    [Multiline]
    public string subtitle_de;
	[Multiline]
    public string body;
    [Multiline]
    public string body_de;
	public Sprite image;

    public CardSettings nextCard;
    public AudioClip audio;
    public AudioClip audio_de;
}
