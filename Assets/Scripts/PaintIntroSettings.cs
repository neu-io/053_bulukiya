using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "PaintIntroSettings" , menuName = "NEEEU/PaintIntro", order = 1)]
public class PaintIntroSettings : ScriptableObject {
	public string title;
    public string title_de;


    [Multiline]
    public string subtitle;
     [Multiline]
    public string subtitle_de;
     [Multiline]
	public string body;
     [Multiline]
    public string body_de;
	public Sprite image;

    public PaintIntroSettings nextCard;
}
