﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPaintIntro : MonoBehaviour{
	public PaintIntro _targetPaintIntro;
	public PaintIntroSettings _PaintIntro;

	public void Set(){
        _targetPaintIntro.gameObject.SetActive(true);
		_targetPaintIntro.SetPaintIntro(_PaintIntro);
	}
    public void Disable(){
        _targetPaintIntro.gameObject.SetActive(false);
		_targetPaintIntro.SetPaintIntro(_PaintIntro);
	}
}
