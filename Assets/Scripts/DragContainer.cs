﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragContainer : MonoBehaviour, IDragHandler {

	public Vector2 _dragDist;
	public Vector2 _dragDistNorm;

	float _lastDrag; 
	RectTransform _element;

	void Start(){
		_element = GetComponent<RectTransform>();
	}

	void Update(){
		if(Time.time - _lastDrag > 0.2f){
			var cur = _element.anchorMin.y;
			cur += _dragDistNorm.y;

			cur = Mathf.Clamp(cur,-1,0);

			_element.anchorMin = new Vector2(_element.anchorMin.x,cur);
			_element.anchorMax = new Vector2(_element.anchorMax.x,cur + 1);
		}
	}
	public void OnDrag(PointerEventData eventData){
		_lastDrag = Time.time;

		_dragDist = eventData.delta;

		var rect = _element.rect;

		_dragDistNorm = new Vector2(_dragDist.x / rect.width, _dragDist.y / rect.height);

		var cur = _element.anchorMin.y;
		cur += _dragDistNorm.y;


		cur = Mathf.Clamp(cur,-1,0);



		_element.anchorMin = new Vector2(_element.anchorMin.x,cur);
		_element.anchorMax = new Vector2(_element.anchorMax.x,cur + 1);


	}
}
