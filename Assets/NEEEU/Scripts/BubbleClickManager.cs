﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NEEEU.MarkerAR
{

    public class BubbleClickManager : MonoBehaviour
    {
        // public GameObject Showspawn;
        // public GameObject Showback;
        // public GameObject Hidebubbles;
        // public GameObject Hidenext;
        // public GameObject Hidetext;
        // public GameObject Hidemap;
        public GameObject PlayaudioSource;

        public Card _cardObject;
        public PaintIntro _paintObject;


        void Update()
        {
            foreach (Touch tch in InputHelper.GetTouches())
            {
                int id = tch.fingerId;

                if (tch.phase != TouchPhase.Began) continue;

                Ray ray = Camera.main.ScreenPointToRay(tch.position);
                RaycastHit hit;

                if (!Physics.Raycast(ray, out hit, 100.0F)) continue;

                Debug.Log("You hit a bubble!"); // Raph: the hit always gets detected as this line shows

                var inst = hit.transform.gameObject.GetComponent<BubbleClick>();

                if(inst == null) continue;

                Debug.Log("We found an instance: ", inst); // On some bubbles, sometimes (?), no instance is found

                if(inst.cardToActivate != null){
                    _cardObject.SetCard(inst.cardToActivate);
                    _cardObject.gameObject.SetActive(true);
                    _paintObject.gameObject.SetActive(false);
                } else if(inst.paintingToActivate != null){
                    _paintObject.SetPaintIntro(inst.paintingToActivate);
                    _paintObject.gameObject.SetActive(true);
                    _cardObject.gameObject.SetActive(false);
                    // PlayaudioSource.GetComponent<AudioSource>().Play();
                } else {
                    Debug.Log("No _cardObject or _paintObject was found.");
                }

                inst.clicked.Invoke();


                // if (hit.transform.gameObject != gameObject) continue;

                // Showspawn.SetActive(true);
                // Showback.SetActive(true);
                // Hidebubbles.SetActive(false);
                // Hidenext.SetActive(false);
                // Hidetext.SetActive(false);
                // Hidemap.SetActive(false);
                // PlayaudioSource.GetComponent<AudioSource>().Play();
                // POI_to_activate.gameObject.SetActive(true);
                // Paint_card_to_deactivate.gameObject.SetActive(false);
                          


            }
        }
    }
}



