﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextClick : MonoBehaviour 
{

    public GameObject map;
    public GameObject bubbles;
    public GameObject next;
    public GameObject nextAR;
    public GameObject begin;

	
	// Update is called once per frame
	void Update () {
        foreach (Touch tch in InputHelper.GetTouches())
        {
            int id = tch.fingerId;

            if (tch.phase != TouchPhase.Began) continue;

            Ray ray = Camera.main.ScreenPointToRay(tch.position);
            RaycastHit hit;
            if (!Physics.Raycast(ray, out hit, 100.0F)) continue;

            if (hit.transform.gameObject != gameObject) continue;

            map.SetActive(true);
            bubbles.SetActive(false);
            next.SetActive(false);
            nextAR.SetActive(false);
            begin.SetActive(true);

        }
    }
}
		
