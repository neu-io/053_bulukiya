﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ARmarkersHub : EditorWindow {

    [MenuItem("NEEEU/AR Markers Hub")]
    public static void ShowWindow(){
        EditorWindow.GetWindow(typeof(ARmarkersHub));
    }

    private string workingFolder = "Assets/HybridReality/ARMarkers";
    private int selectedIndex = 0;
    private ARReferenceImagesSet selectedSet;

    //[SerializeField]
    private string[] referenceImageSets = new string[]{};
    string[] guids;
    [SerializeField]
    public ARReferenceImage[] images;


    void Awake()
    {
        if (!AssetDatabase.IsValidFolder(workingFolder))
        {
            AssetDatabase.CreateFolder("Assets/HybridReality","ARMarkers");
            //ARReferenceImagesSet defaultSet = new ARReferenceImagesSet();
            ARReferenceImagesSet defaultSet = ScriptableObject.CreateInstance<ARReferenceImagesSet>();
            defaultSet.resourceGroupName = "default";
            AssetDatabase.CreateAsset(defaultSet, workingFolder + "/defaultARReferenceImageSet.asset");

        }


        guids = AssetDatabase.FindAssets("t:ARReferenceImagesSet");

        //we cant resize builtin arrays, so we have to recreate them
        referenceImageSets = new string[guids.Length];
        for (int i = 0; i < guids.Length; i++)
        {
            referenceImageSets[i] = AssetDatabase.GUIDToAssetPath(guids[i]);
        }

    }


    void OnGUI(){

        EditorGUILayout.LabelField( "neeeu.io AR Markers Hub");
        EditorGUILayout.Space();


        ImageSetDropDown();
        EditorGUILayout.Space();

        //EditorGUILayout.LabelField("Resource Group name: " + selectedSet.resourceGroupName);
        selectedSet.resourceGroupName = EditorGUILayout.TextField("ResourceGroup name: ", selectedSet.resourceGroupName);
        EditorGUILayout.Space();

        dropZone();
        EditorGUILayout.Space();


        showImageList();


    }

    /// <summary>
    /// Select the AR Reference Image Set we are working on
    /// </summary>
    void ImageSetDropDown(){
        
        if (referenceImageSets.Length > 0)
        {
            selectedIndex = EditorGUILayout.Popup("Reference Image Set: ", selectedIndex, referenceImageSets);
            selectedSet = AssetDatabase.LoadAssetAtPath<ARReferenceImagesSet>(referenceImageSets[selectedIndex]);
        }

        //Debug.Log(selectedIndex);

    }



    /// <summary>
    /// Drops the zone. Drag and drop area for 2D Textures
    /// </summary>
    void dropZone(){

        Event evt = Event.current;
        Rect dropRect = GUILayoutUtility.GetRect(0.0f, 100.0f, GUILayout.ExpandWidth(true) );
        GUI.Box(dropRect, "Drop 2D Textures Here");

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!dropRect.Contains(evt.mousePosition))
                {
                    return;
                }

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                
                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();

                    int draggedAmount = DragAndDrop.objectReferences.Length;
                    foreach (var dragged_object in DragAndDrop.objectReferences)
                    {
                        string assetPath = workingFolder + "/" + dragged_object.name + ".asset";
                        //TODO point 2
                        if (dragged_object is Texture2D && !System.IO.File.Exists(assetPath))
                        {
                            //Debug.Log(dragged_object.name);
                            ARReferenceImage image = ScriptableObject.CreateInstance<ARReferenceImage>();
                            image.imageName = dragged_object.name;
                            image.imageTexture = dragged_object as Texture2D;
                            image.physicalSize = 0.25f;
                            AssetDatabase.CreateAsset(image, assetPath);
                            //so theres no append in arrays in C#
                            ARReferenceImage[] originalArray = selectedSet.referenceImages;
                            ARReferenceImage[] tempArray;
                            //manage empty array special case
                            if (originalArray == null || originalArray.Length == 0)
                            {
                                tempArray = new ARReferenceImage[1];
                                tempArray[0] = AssetDatabase.LoadAssetAtPath<ARReferenceImage>(assetPath);
                         
                            } else
                            {
                                tempArray = new ARReferenceImage[originalArray.Length + 1];

                                for (int i = 0; i < originalArray.Length; i++)
                                {
                                    tempArray[i] = originalArray[i];
                                }
                                tempArray[tempArray.Length - 1] = AssetDatabase.LoadAssetAtPath<ARReferenceImage>(assetPath);
                            }


                            selectedSet.referenceImages = tempArray;

                        } else if (dragged_object is ARReferenceImage)
                        {
                            //so theres no append in arrays in C#
                            ARReferenceImage[] originalArray = selectedSet.referenceImages;
                            ARReferenceImage[] tempArray;
                            //manage empty array special case
                            if (originalArray == null || originalArray.Length == 0)
                            {
                                tempArray = new ARReferenceImage[1];
                                tempArray[0] = dragged_object as ARReferenceImage;

                            } else
                            {
                                tempArray = new ARReferenceImage[originalArray.Length + 1];
                                for (int i = 0; i < originalArray.Length; i++)
                                {
                                    tempArray[i] = originalArray[i];
                                }
                                tempArray[tempArray.Length - 1] = dragged_object as ARReferenceImage;
                            }

                            selectedSet.referenceImages = tempArray;

                        } else if (dragged_object is ARReferenceImagesSet)
                        {
                            //select it on the popup??
                        }

                    }
                }

                break;
        }

    }


    /// <summary>
    /// Lists the images inside the set.
    /// </summary>
    void showImageList(){

        Vector2 scrollStart = new Vector2(0.0f, 100.0f);

        scrollStart = EditorGUILayout.BeginScrollView(scrollStart);
        foreach (var img in selectedSet.referenceImages)
        {
            EditorGUILayout.ObjectField(img.name, img, typeof(ARReferenceImage));
        }

        EditorGUILayout.EndScrollView();

    }


}


///////ToDo////////
/// 
/// 
/// 1. 
/// 
/// 2. Case when the ARReference exists but its not in the array
/// 
/// 3. Create a + button to create a new Image Set (Refactor to work with lists?)
/// 
/// 4. Create prefab from marker 
/// 
/// 
/// 