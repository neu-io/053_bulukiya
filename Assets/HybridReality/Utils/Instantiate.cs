﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate : MonoBehaviour {
	public void Spawn(GameObject prefab){
		Instantiate(prefab,Camera.main.transform.position + Camera.main.transform.forward*0.25f,Quaternion.identity);
	}
}
