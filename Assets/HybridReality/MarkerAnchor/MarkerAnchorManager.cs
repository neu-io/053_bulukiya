﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace NEEEU.HybridReality{

	public class MarkerAnchorManager : MonoBehaviour {

        public ARReferenceImagesSet anchorImagesSet;

        [Header("Debug Options")]
        //public bool enableDebug = true;
        public bool enableInst = true;
		public GameObject prefabToInstantiate;
        public static MarkerAnchorManager manager;
        public static List<MarkerAnchor> mList = new List<MarkerAnchor>();

		void Start () {
			if(manager != null){
				//Debug.LogError("Double marker managers");
                Object.Destroy(this);
				return;
			}

			manager = this;

			UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AnchorAdded;
		}


		void AnchorAdded(ARImageAnchor arImageAnchor){
			//Debug.Log("Marker detected:"+arImageAnchor.referenceImageName);
            foreach(MarkerAnchor mInst in mList){
				if(arImageAnchor.referenceImageName != mInst.referenceImage.imageName){
                    //mInst.enableDebug = enableDebug;
					continue;
				}
				mInst.AnchorAdded(arImageAnchor);
                //Debug.Log("anchor image: " + mInst.name + " in list.");
				return;
			}
			if(!enableInst){
				return;
			}
			InstMarker(arImageAnchor);
			UnityARSessionNativeInterface.GetARSessionNativeInterface().RemoveUserAnchor(arImageAnchor.identifier);
		}

		void InstMarker(ARImageAnchor arImageAnchor){
			Vector3 position = UnityARMatrixOps.GetPosition (arImageAnchor.transform);
			Quaternion rotation = UnityARMatrixOps.GetRotation (arImageAnchor.transform);

            GameObject marker = Instantiate(prefabToInstantiate, position, rotation);
            

            marker.name = "Marker: "+arImageAnchor.referenceImageName;
            MarkerAnchor mInst = marker.GetComponent<MarkerAnchor>();
			mInst.referenceImageStr = arImageAnchor.referenceImageName;
            foreach(ARReferenceImage rInst in anchorImagesSet.referenceImages){
				if(rInst.imageName != arImageAnchor.referenceImageName){
					continue;
				}
				mInst.referenceImage = rInst;
				break;
			}
			
		}


	}
}