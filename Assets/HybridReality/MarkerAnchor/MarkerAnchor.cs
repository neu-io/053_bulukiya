﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.iOS;

//Script to be used in any Image Anchor object you want to recognize
//the manager holds a list of markers and each object with this component will add itself to the list

namespace NEEEU.HybridReality{

    public enum AnchorType { WorldOrigin, WorldOffset, Local}
    public class MarkerAnchor : MonoBehaviour {
        public bool triggerActivate;
		
        public AnchorType anchorType = AnchorType.Local;
		public ARReferenceImage referenceImage;
		public string referenceImageStr;
        public UnityEvent detected;
        public UnityEvent firstTimeDetected;
        private bool firstTime = true;

        [Header("Debug Options")]
		public bool enableDebug;
		public Material Active;
		public Material InActive;
		public GameObject debugPrefab;

		float lastUpdate;
		//Vector4 referenceCuma;
		Vector3 referencePos;
		Quaternion referenceRot;
        Vector3 realWorldPos;
        Quaternion realWorldRot;


		void Start(){
			if(referenceImage == null && referenceImageStr == ""){
				return;
			}
			if(referenceImageStr == ""){
				referenceImageStr = referenceImage.imageName;
			}
			if(referenceImage == null){
                if(MarkerAnchorManager.manager == null){
					Debug.LogError("No manager found!");
					return;
				}
                foreach(ARReferenceImage rInst in MarkerAnchorManager.manager.anchorImagesSet.referenceImages){
					if(rInst.imageName != referenceImageStr){
						continue;
					}
					referenceImage = rInst;
				}
				if(referenceImage == null){
					Debug.LogError("Image not found!");
					return;
				}
			}

            if (Active == null )
            {
                Active = new Material(Shader.Find("Standard"));
                Active.color = Color.white;
            }
            if (InActive == null)
            {
                InActive = new Material(Shader.Find("Standard"));
                InActive.color = Color.black;
            }

            MarkerAnchorManager.mList.Add(this);
			//transform.localScale = new Vector3(referenceImage.physicalSize,referenceImage.physicalSize,referenceImage.physicalSize);
            referencePos = transform.position;
            referenceRot = transform.rotation;
		}


		void Update(){
            if(triggerActivate){
                detected.Invoke();
                triggerActivate = false;
            }
			if (enableDebug)
            {
                GetComponent<Renderer>().material = Time.time - lastUpdate < .25 ? Active : InActive;
            }
		}
		void OnDestroy(){
            MarkerAnchorManager.mList.Remove(this);
		}

		public void AnchorAdded(ARImageAnchor arImageAnchor){

            //Debug.Log("Image Anchor triggered");
			if(arImageAnchor.referenceImageName != referenceImageStr){
                //Debug.Log("Not my image");
				return;
			}


			Vector3 position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            Quaternion rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

            if(enableDebug){
                lastUpdate = Time.time;
                Instantiate(debugPrefab, position, rotation);
            }
                

            if(anchorType == AnchorType.Local){
                
				transform.position = position;
				transform.rotation = rotation;
                Debug.Log("Local anchor: " + arImageAnchor.referenceImageName);

			}else if (anchorType == AnchorType.WorldOrigin){
                
                GameObject originObject = new GameObject();
                originObject.transform.position = position;
                originObject.transform.rotation = rotation;

                UnityARSessionNativeInterface.GetARSessionNativeInterface().SetWorldOrigin(originObject.transform);
                Debug.Log("World anchor: " + arImageAnchor.referenceImageName + " at pos: " + position);

			} else if (anchorType == AnchorType.WorldOffset){
                
                position -= transform.position;
                // Why: https://stackoverflow.com/questions/22157435/difference-between-the-two-quaternions
                rotation = rotation * Quaternion.AngleAxis(0, Vector3.up) * Quaternion.Inverse(transform.rotation);
                float yRot = rotation.eulerAngles.y;

                // filter high frequency noise from image detection aka low pass filter
                if (yRot < 2 || yRot > 360 - 2)
                {
                    yRot = 0;
                }

                GameObject originObject = new GameObject();
                originObject.transform.position = position;
                originObject.transform.rotation = Quaternion.Euler(new Vector3(0, yRot, 0));
                UnityARSessionNativeInterface.GetARSessionNativeInterface().SetWorldOrigin(originObject.transform);
                Debug.Log("Offset anchor " + arImageAnchor.referenceImageName + " at pos: " + position);
            }

			detected.Invoke();
            if (firstTime)
            {
                firstTimeDetected.Invoke();
                firstTime = false;
            }
            UnityARSessionNativeInterface.GetARSessionNativeInterface().RemoveUserAnchor(arImageAnchor.identifier);
		}
		
	}
}

