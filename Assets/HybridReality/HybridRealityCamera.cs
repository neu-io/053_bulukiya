﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using NEEEU;
using NEEEU.HybridReality;

[RequireComponent(typeof(Camera))]
public class HybridRealityCamera : MonoBehaviour {

    Camera cam;
    private UnityARVideo aRVideo;
    public Material clearMaterial;
    private UnityARCameraNearFar camNear;
    private UnityARCameraManager cameraManager;
    [Header("AR Session Options")]
    public UnityARAlignment startAlignment = UnityARAlignment.UnityARAlignmentGravity;
    public UnityARPlaneDetection planeDetection = UnityARPlaneDetection.None;
    public bool getPointCloud = true;
    public bool enableLightEstimation = false;
    public bool enableAutoFocus = true;
    [Header("Markers Options")]
    public ARReferenceImagesSet detectionImages = null;
    private MarkerAnchorManager markerManager;
    //public bool enableDebug = true;
    public bool enableInstancing = true;
    public GameObject prefabToInstantiate;



	// Use this for initialization
	void Start () {

        cam = GetComponent<Camera>();
        cam.clearFlags = CameraClearFlags.Depth;
        cam.nearClipPlane = 0.01f;
        cam.depth = -1;
        cam.tag = "MainCamera";
        aRVideo = gameObject.AddComponent<UnityARVideo>();//  new UnityARVideo();
        if (clearMaterial == null) clearMaterial = new Material(Shader.Find("Unlit/ARCameraShader"));//
        aRVideo.m_ClearMaterial = clearMaterial;
        camNear = gameObject.AddComponent<UnityARCameraNearFar>();// new UnityARCameraNearFar();
        cameraManager = gameObject.AddComponent<UnityARCameraManager>();// new UnityARCameraManager();
        cameraManager.m_camera = cam;
        cameraManager.startAlignment = startAlignment;
        cameraManager.planeDetection = planeDetection;
        cameraManager.getPointCloud = getPointCloud;
        cameraManager.enableLightEstimation = enableLightEstimation;
        cameraManager.enableAutoFocus = enableAutoFocus;
        cameraManager.detectionImages = detectionImages;
        markerManager = gameObject.AddComponent<MarkerAnchorManager>();
        markerManager.anchorImagesSet = detectionImages;
        //markerManager.enableDebug = enableDebug;
        markerManager.enableInst = enableInstancing;
        markerManager.prefabToInstantiate = prefabToInstantiate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
