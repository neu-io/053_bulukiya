﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NEEEU.HybridReality{
	public class SaveTransform : MonoBehaviour {
		public GameObject prefab;
		public uint prefabid;
		public string id;

		void Start () {
			if(id == ""){
				id = System.Guid.NewGuid().ToString();
			}
			
		}
		
		void Update () {
			
		}
		public void Save(string dirpath){
			if(id == ""){
				Debug.LogError("No id given");
				return;
			}
			
			foreach(KeyValuePair<Type, Type> entry in SaveAble.saveTypes){
				Component cmp = GetComponent(entry.Key);
				if(cmp == null){
					continue;
				}

				SaveAble saveObj =  (SaveAble) Activator.CreateInstance(entry.Value);
				saveObj.Save(cmp);

				Debug.Log(fileNameCmp(dirpath,entry.Key));
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Create (fileNameCmp(dirpath,entry.Key));
				bf.Serialize (file, saveObj);
				file.Close ();
			}
		}
		public void Load(string dirpath){
			if(id == ""){
				Debug.LogError("No id given");
				return;
			}

			foreach(KeyValuePair<Type, Type> entry in SaveAble.saveTypes){
				Component cmp = GetComponent(entry.Key);
				if(cmp == null){
					continue;
				}
				string filename = fileNameCmp(dirpath,entry.Key);
				if(!File.Exists (filename)){
					continue;
				}
				
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (filename, FileMode.Open);
				SaveAble saveObj =  (SaveAble) bf.Deserialize (file);
				saveObj.Load(cmp);

			}
		}
		string fileName(string path){
			return path + "/"+id+".dat";
		}
		string fileNameCmp(string path,Type type){
			return path + "/"+id+"-"+type.FullName+".dat";
		}	
	}
	[System.Serializable]
	public class SaveAble{
		public static Dictionary<Type,Type> saveTypes = new Dictionary<Type,Type>(){
			{typeof(Transform),typeof(SaveTransformData)},
            {typeof(MarkerAnchor),typeof(SaverMarkerData)},
			{typeof(NetworkMarker),typeof(SaverNetworkMarkerData)}
		};
		public virtual void Save(Component cpm){
			Debug.Log("Saving cmp");
		}
		public virtual void Load(Component cpm){
			Debug.Log("Loading cmp");
		}
	}
	[System.Serializable]
	public class SaveTransformData : SaveAble{
		float x;
		float y;
		float z;
		float rot_x;
		float rot_y;
		float rot_z;
		float rot_w;
		public override void Save(Component cpm){
			Debug.Log("Saving transform");
			Transform transform = cpm as Transform;
			x = transform.localPosition.x;
			y = transform.localPosition.y;
			z = transform.localPosition.z;

			rot_x = transform.localRotation.x;
			rot_y = transform.localRotation.y;
			rot_z = transform.localRotation.z;
			rot_w = transform.localRotation.w;
		}
		public override void Load(Component cpm){
			Debug.Log("Loading transform");
			Transform transform = cpm as Transform;
			transform.localPosition = new Vector3(x,y,z);

			transform.localRotation = new Quaternion(rot_x,rot_y,rot_z,rot_w);
		}
	}
	[System.Serializable]
	public class SaverMarkerData : SaveAble{
		string markerName;
		public override void Save(Component cpm){
            MarkerAnchor marker = cpm as MarkerAnchor;
			markerName = marker.referenceImageStr;
		}
		public override void Load(Component cpm){
            MarkerAnchor marker = cpm as MarkerAnchor;
            if(MarkerAnchorManager.manager == null){
				Debug.LogError("No marker manager available");
				return;
			}
            foreach(ARReferenceImage rInst in MarkerAnchorManager.manager.anchorImagesSet.referenceImages){
				if(rInst.imageName != markerName){
					continue;
				}
				marker.referenceImage = rInst;
				marker.referenceImageStr = rInst.imageName;
				return;
			}
			Debug.LogError("Marker reference not found!");
		}
	}
	[System.Serializable]
	public class SaverNetworkMarkerData : SaveAble{
		string markerName;
		public override void Save(Component cpm){
			NetworkMarker marker = cpm as NetworkMarker;
			markerName = marker.referenceImageStr;
		}
		public override void Load(Component cpm){
			NetworkMarker marker = cpm as NetworkMarker;
			marker.referenceImageStr = markerName;
		}
	}
}