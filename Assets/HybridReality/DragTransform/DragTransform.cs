﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.iOS;

namespace NEEEU.HybridReality{
	public class DragTransform : MonoBehaviour {
		public GameObject dragger;
		Matrix4x4 offset;

		DragTransform parent;
		public UnityEvent connected;
		public UnityEvent disconnected;
		void Start(){
			parent = GetComponentsInParent<DragTransform>().Last();
			if(parent == this){
				parent = null;
			}
		}
		void Update(){
			if(dragger == null){
				return;
			}

			Matrix4x4 newPos = Matrix4x4.TRS(dragger.transform.position,dragger.transform.rotation,Vector3.one) * offset;
			transform.position = Utils.GetPosition (newPos);
			transform.rotation = Utils.GetRotation (newPos);
		}
		public void Connect(GameObject obj){
			if(dragger != null){
				return;
			}
			if(parent != null){
				parent.Connect(obj);
				return;
			}

			offset = Matrix4x4.TRS(obj.transform.position,obj.transform.rotation,Vector3.one).inverse * Matrix4x4.TRS(transform.position,transform.rotation,Vector3.one);
			dragger = obj;
			connected.Invoke();
		}
		public void Disconnect(){
			if(parent != null){
				parent.Disconnect();
				return;
			}
			dragger = null;
			disconnected.Invoke();
		}
	}
}