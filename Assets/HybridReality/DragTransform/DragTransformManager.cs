﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NEEEU.HybridReality{
	public class DragTransformManager : MonoBehaviour {
		public GameObject connectPrefab;
		void Update(){
			foreach(Touch tch in InputHelper.GetTouches()){
				int id = tch.fingerId;
				
				if (tch.phase != TouchPhase.Began) continue;
				if (IsOverGUI(tch.position)) continue;

				Ray ray = Camera.main.ScreenPointToRay(tch.position);
				RaycastHit hit;
				if (!Physics.Raycast (ray, out hit, 100.0F)) continue;

				DragTransform drg = hit.transform.gameObject.GetComponent<DragTransform>();
				if(drg == null) continue;

				GameObject drgtcher = Instantiate(connectPrefab);
				DragTransformTouch drgtch = drgtcher.GetComponent<DragTransformTouch>();
				drgtch.Connect(id,hit.transform.gameObject);

			}
		}
		bool IsOverGUI(Vector2 pos){
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = pos;
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
			return results.Count > 0;
		}
	}
}