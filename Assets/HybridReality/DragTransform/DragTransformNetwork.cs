﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace NEEEU.HybridReality{
	public class DragTransformNetwork : NetworkBehaviour {
		DragTransform drgInst;
		[SyncVar]
		public NetworkInstanceId playerId = NetworkInstanceId.Invalid;
		bool isHeld;
		// Use this for initialization
		void Start () {
			drgInst = GetComponent<DragTransform>();
			drgInst.connected.AddListener(Connected);
			drgInst.disconnected.AddListener(Disconnected);

			// connect this object ot the dragtransform instance
		}
		void Connected(){
			// if connected by a local player say to the server that this objects is picked up by me
			// if picked up by a gameobject with a networkphone attaced ignore	
			// if it picks up a note, it should communicate to the server that it picket it up.
			// Debug.Log("picked up object!"+NetworkPhone.localPlayerId);
			if(playerId != NetworkInstanceId.Invalid){
				return;
			}
			isHeld = true;
			// NetworkPhone.localPlayer.CmdStartDrag(GetComponent<NetworkIdentity>().netId);
		}
		void Update(){
			// if(drgInst.dragger == null && playerId != NetworkInstanceId.Invalid && playerId != NetworkPhone.localPlayerId){
			// 	drgInst.Connect(ClientScene.FindLocalObject(playerId));
			// }
			// if(drgInst.dragger != null && playerId == NetworkInstanceId.Invalid && !isHeld){
			// 	drgInst.Disconnect();
			// }
		}

		void Disconnected(){
			// if(playerId != NetworkPhone.localPlayerId){
			// 	return;
			// }
			// isHeld = false;
			// Debug.Log("Disconnected drag");
			// NetworkPhone.localPlayer.CmdStopDrag(GetComponent<NetworkIdentity>().netId);
			// sync the final position :)
		}

		
	}
}