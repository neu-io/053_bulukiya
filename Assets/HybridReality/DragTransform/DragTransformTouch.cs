﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NEEEU.HybridReality{
	public class DragTransformTouch : MonoBehaviour {
		int fingerid = -1;
		DragTransform connection;
		Vector2 startPos;
		
		public UnityEvent connected;
		public UnityEvent disconnected;
		public void Connect(int fingerid, GameObject obj){
			Debug.Log("Connect");
			this.fingerid = fingerid;
			name = obj.name +"-OFFSET";

			Update();

			connection = obj.GetComponent<DragTransform>();
			connection.Connect(gameObject);
			connected.Invoke();
			disconnected.Invoke();
		}
		public void Disconnect(){
			Debug.Log("Disconnect");
			Destroy(gameObject);
			connection.Disconnect();
			connection = null;
		}
		void Update(){
			if(fingerid < 0){
				Disconnect();
				return;
			}
			Touch tch = InputHelper.GetTouch(fingerid);
			if(tch.phase == TouchPhase.Ended || tch.phase == TouchPhase.Canceled){
				Disconnect();
				return;
			}

			Camera mainCam = Camera.main;

			Vector3 dir = mainCam.ScreenPointToRay(tch.position).direction;
			Quaternion rot = mainCam.transform.rotation;
			rot = Quaternion.FromToRotation(mainCam.gameObject.transform.forward,dir) * rot;
			
			transform.position = mainCam.gameObject.transform.position;
			transform.rotation = rot;
		}
	}
}
