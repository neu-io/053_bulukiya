﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkManagerHelper : MonoBehaviour {

	void Start(){
        
		NetworkManager mang = GetComponent<NetworkManager>();
		#if !UNITY_EDITOR

			Debug.Log("Connecting");
			mang.StartClient();
		#else
			Debug.Log("Hosting");
			mang.StartHost();
		#endif
	}
}
