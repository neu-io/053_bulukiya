﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace NEEEU.HybridReality{
	public class NetworkMarker : NetworkBehaviour {
		[SyncVar (hook = "OnChangeRef")]
		public string referenceImageStr;
		void Awake(){
            GetComponent<MarkerAnchor>().enabled = false;
		}
		void Start(){
            GetComponent<MarkerAnchor>().referenceImageStr = referenceImageStr;
            GetComponent<MarkerAnchor>().enabled = true;
		}
		void OnChangeRef(string reference){
            GetComponent<MarkerAnchor>().referenceImageStr = reference;
            GetComponent<MarkerAnchor>().enabled = true;
		}
	}
}
