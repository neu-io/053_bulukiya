﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//To Use with with Vive trackers

namespace NEEEU.HybridReality{
    public class NetworkMarkerManager : MarkerAnchorManager {
		public GameObject marker1;
		public GameObject tracker1;

		public GameObject tracker2;
		public ARReferenceImage marker2;
		void Update(){
			if(marker1 == null){
				return;
			}
			if(marker2 == null){
				return;
			}
			if(tracker1 == null){
				return;
			}
			if(tracker2 == null){
				return;
			}
			if(!tracker1.activeInHierarchy){
				return;
			}
			if(!tracker2.activeInHierarchy){
				return;
			}

			

            MarkerAnchor mInst2 = getByReference(marker2);
			if(mInst2 == null){
				mInst2 = initMarker(marker2);
			}

			Matrix4x4 offset = tracker1.transform.localToWorldMatrix.inverse * tracker2.transform.localToWorldMatrix;

			// GameObject offsetObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
			// offsetObj.name = "Tracker offset";
			// offsetObj.transform.position = Utils.GetPosition(offset);
			// offsetObj.transform.rotation = Utils.GetRotation(offset);
			// offsetObj.transform.localScale = Vector3.one * 0.05f;

			Matrix4x4 mrk1 = Matrix4x4.TRS(marker1.transform.position,marker1.transform.rotation,Vector3.one);

			offset = mrk1 * offset;

			// GameObject finalObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
			// finalObj.name = "Final offset";
			// finalObj.transform.position = Utils.GetPosition(offset);
			// finalObj.transform.rotation = Utils.GetRotation(offset);
			// finalObj.transform.localScale = Vector3.one * 0.05f;

			mInst2.transform.position = Utils.GetPosition (offset);
			mInst2.transform.rotation = Utils.GetRotation (offset);

			marker1 = null;
			marker2 = null;

		}
        MarkerAnchor getByReference(ARReferenceImage arimage){
            foreach(MarkerAnchor mInst in mList){
				if(arimage.imageName == mInst.referenceImageStr){
					return mInst;
				}
			}
			return null;
		}
        MarkerAnchor initMarker(ARReferenceImage arimage){
            GameObject marker = Instantiate(prefabToInstantiate);
			marker.GetComponent<NetworkMarker>().referenceImageStr = arimage.imageName;

			marker.name = "Marker: "+arimage.imageName;
            MarkerAnchor mInst = marker.GetComponent<MarkerAnchor>();
			// mInst.referenceImage = arimage;
			// mInst.referenceImageStr = arimage.imageName;
			//mInst.enableDebug = enableDebug;

			NetworkServer.Spawn(marker);
			return mInst;
		}
	}
}