﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//[ExecuteInEditMode]
public class PNG_screenshot : MonoBehaviour {

    private Material material;

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            StartCoroutine("savePNG");
            Debug.Log("pressed space");
        }

        material = GetComponent<fullscreenFX>().material;
    }

    public IEnumerator savePNG()
    {
        // We should only read the screen buffer after rendering is complete
        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        byte[] bytes = tex.EncodeToPNG();
        Object.DestroyImmediate(tex);

        string fullPath = Application.dataPath + "/Marker_" + material.GetFloat("_Seed") + ".png";

        if (!File.Exists(fullPath))
        {
            File.WriteAllBytes(fullPath, bytes);
        }
    }

    public IEnumerator savePNGandCreateMarker(){
        
        // We should only read the screen buffer after rendering is complete
        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        byte[] bytes = tex.EncodeToPNG();
        Object.DestroyImmediate(tex);

        string fullPath = Application.dataPath + "/Marker_" + material.GetFloat("_Seed") + ".png";

        if (!File.Exists(fullPath))
        {
            File.WriteAllBytes(fullPath, bytes);
        }
    }


    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(1024, 1024);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
}
