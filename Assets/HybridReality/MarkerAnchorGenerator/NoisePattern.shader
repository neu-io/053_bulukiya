﻿Shader "NEEEU/NoisePattern"
{
	Properties
	{
		_Freq ("Frequency", Float) = 1
        _Seed ("Seed", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
            #include "noiseSimplex.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
            uniform float
            _Freq,
            _Seed
            ;

            const float2x2 m = float2x2( 0.80,  0.60, -0.60,  0.80 );

            float noise( in fixed2 x )
            {
                return sin(1.5*x.x)*sin(1.5*x.y);
            }

            float fbm4( fixed2 p )
            {
                float f = 0.0;
                f += 0.5000*snoise( p );
                p = mul(m,p)*2.02;
                f += 0.2500*snoise( p );
                p = mul(m,p)*2.03;
                f += 0.1250*snoise( p );
                p = mul(m,p)*2.01;
                f += 0.0625*snoise( p );
                return f/0.9375;
            }

            float pattern(inout fixed2 p, in float t, out fixed2 r){

                
                r = fbm4(p - t);
                return fbm4(p + t);

            }


			fixed4 frag (v2f i) : SV_Target
			{
                fixed2 uv = i.uv * _Freq;
				fixed4 col = fixed4(0.0, 0.0, 0.0, 1.0);
                int c;
                fixed2 r;
                for(c = 0; c < 3; c++){

                    _Seed += 0.75;
                    //col *= sqrt(col);
                    col[c] += noise(uv + _Seed) / 2 + 0.5f;
       
                    col[c] += frac(pattern(uv, _Seed, r)) * 0.75;
                    col[c] /= pow(col, 0.05);
                    col[c] *= pow(col, r.x);
                    //col[c] /= length(uv*2.0 - 05) * 0.3;
                    //col[c] *= step(0.5, col);
               
                }


				return col;
			}
			ENDCG
		}
	}
}
