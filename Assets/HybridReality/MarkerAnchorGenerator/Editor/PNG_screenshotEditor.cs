﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
[CustomEditor(typeof(PNG_screenshot))]
public class PNG_screenshotEditor : Editor {

    public override void OnInspectorGUI(){

        PNG_screenshot screenshoter = (PNG_screenshot)target;

        EditorGUILayout.HelpBox("Enter Play mode and Press 'space' to save a png to the Assets folder", MessageType.Info);

        //to do make coroutines work in the editor
        //if (GUILayout.Button("Take Screenshot"))
        //{
        //    screenshoter.StartCoroutine("savePNG");
        //}

    }
}
