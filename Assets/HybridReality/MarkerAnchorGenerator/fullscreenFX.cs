﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class fullscreenFX : MonoBehaviour {

    public Material material;
    public float randomSeed;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_Seed", randomSeed);
        Graphics.Blit(source, destination, material);
    }



}
